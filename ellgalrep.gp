install(roots_to_pol, "Gn");

\\ Matrix of the algebra endomorphism Q[x]/(f) -> Q[y]/(g)
\\ defined by sending x to a (assuming g | f(a)).
homomorphism_matrix(f, g, a) =
{
   my(d = poldegree(f), e = poldegree(g));
   a = Mod(a, g);
   Mat([Colrev(liftpol(b), e) | b <- powers(a, d - 1)]);
}

\\ LLL-reduced integral basis of Q[x]/(f) as [factors, basis, P],
\\ where factors is a list of all factors of f, basis is a vector
\\ of integral bases of Q[x]/(g) for each g in factors, and P is
\\ the basis transformation matrix from the power basis of
\\ Q[x]/(f) to the integral basis.
reduced_integral_basis(f, pr) =
{
   my(factors = factor(f)[,1],
      fields = [nfinit([g, pr], 3) | g <- factors],
      P = matconcat([K[1][8] * homomorphism_matrix(f, K[1].pol, K[2]) |
                     K <- fields]~));
   [[K[1].pol | K <- fields], [K[1].zk | K <- fields], P];
}

\\ Element of A \otimes A encoding the Weil pairing, i.e. the
\\ image of z under Q[z, z^-1] -> A \otimes A.  Explicitly:
\\ theta_pol(alpha_v, alpha_w) = zeta_m^(v[1]*w[2] - v[2]*w[1])
theta_pol(values, zeta_m) =
{
   my(m = matsize(values)[1],
      v = variable(values),
      z = powers(zeta_m, m - 1),
      z1, z2, A, W);
   if(v == 0, v = 'x);
   z2 = varhigher("z2", v);
   z1 = varhigher("z1", z2);
   A = matrix(m, m, i0, j0,
              W = matrix(m, m, i, j,
                         z[((i0-1)*(j-1) - (j0-1)*(i-1)) % m + 1]);
              polinterpolate(concat(Vec(values)), concat(Vec(W)), z2));
   return(simplify(liftall(polinterpolate(concat(Vec(values)),
                                          concat(Vec(A)), z1))));
}

\\ Coefficients of theta with respect to power basis (z1^i z2^j)
\\ (matrix of bilinear map A^\vee x A^\vee -> Q w.r.t dual basis):
\\ theta_pol = \sum_{i,j} theta_power[i,j] z1^i z2^j
theta_power(values, zeta_m) =
{
   my(m = matsize(values)[1],
      p = characteristic(zeta_m),
      pol = theta_pol(values, zeta_m), z1, z2, th);
   [z1, z2] = variables(pol);
   th = matrix(m^2, m^2, i, j,
               polcoeff(polcoeff(pol, i - 1, z1), j - 1, z2));
   th = Mod(eval(Str(th)), p);  \\ hack to get rid of t_FFELT
}

\\ Matrix of the values of the given rational function on the points
\\ of  E[m]  over a suitable extension of  F_p  relative to some basis,
\\ together with the value of the Weil pairing on the basis vectors.
\\ The function must be injective and nowhere zero on  E[m] \ {0}.
values_and_weil_pairing(E, m, function, p) =
{
   my(d, P_m, g, k, roots, pts, P, Q, zeta_m, points, values);
   P_m = elldivpol(E, m);
   d = 2*lcm([poldegree(f) | f <- factormod(P_m, p)[,1]]);
   if(d >= max(10, m), error("extension degree too large"));
   g = ffinit(p, d, 'y);
   k = ffgen(g, 'a);
   E = ellinit(E, k);
   roots = polrootsff(P_m*k^0);
   for(i = 1, #roots,
      P = [roots[i], ellordinate(E, roots[i])[1]];
      for(j = i + 1, #roots,
         Q = [roots[j], ellordinate(E, roots[j])[1]];
         zeta_m = ellweilpairing(E, P, Q, m);
         if(fforder(zeta_m, m) == m, break(2))));
   points = matrix(m, m, i, j,
                   elladd(E, ellmul(E, P, i - 1), ellmul(E, Q, j - 1)));
   values = apply(P -> if(P == [0], 0*k,
                          substvec(function, ['x, 'y], P)), points);
   return([values, zeta_m]);
}

\\ Logarithmic height of  x  (rational, polynomial, vector or matrix).
height(x) =
{
   if(x == [], 0,
      type(x) == "t_POL", vecmax(apply(self(), Vec(x))),
      type(x) == "t_MAT" || type(x) == "t_VEC", vecmax(apply(self(), x)),
      log(max(abs(numerator(x)), denominator(x))));
}

\\ As ellgalrep(E, m), using the specified rational function.
ellgalrep_function(E, m, function) =
{
   my(A_p, f_N = [], f_p, f = [], map = matrix(0, 2), p = 1,
      r = 0, s = 0, N = 1, B, P, P_p, values, zeta_m,
      Phi_N = [], Phi_p, Phi = [], i);
   write1("/dev/stderr", "computing f:");
   \\ The criteria in the following loop are slighly arbitrary, but
   \\ work well in practice.
   while(f == [] || s < 5
         || height(f) > log(N)/8,
         p = nextprime(p + 1);
         if(m % p == 0, next);
         write1("/dev/stderr", " ", p);
         if(p > 100 && r == 0,
            error("the function ", function, " appears to be non-injective"));
         A_p = iferr(values_and_weil_pairing(E, m, function, p),
                     err, next);
         f_p = Mod(eval(Str(roots_to_pol(concat(Vec(A_p[1])), 't))), p);
         map = matconcat([map, [p, A_p]]~);
         f_N = if(f_N == [], f_p, chinese(f_N, f_p));
         N *= p;
         r++;
         f = bestappr(f_N);
         if(f != [], s++, s = 0));
   write("/dev/stderr", "\ncomputing integral basis");
   B = reduced_integral_basis(f, factor(E.disc * m)[,1]~);
   P = B[3]^-1;
   N = 1;
   i = 0;
   s = 0;
   write1("/dev/stderr", "computing Phi:");
   while(i < matsize(map)[1] &&
         (Phi == [] || s < 5
          || height(Phi) > log(N)/8),
         i++;
         p = map[i, 1];
         write1("/dev/stderr", " ", p);
         P_p = iferr(Mod(P, p), err, next);
         [values, zeta_m] = map[i, 2];
         Phi_p = P_p~ * theta_power(values, zeta_m)~^-1 * P_p;
         Phi_N = if(Phi_N == [], Phi_p, chinese(Phi_N, Phi_p));
         N *= p;
         Phi = bestappr(Phi_N);
         if(Phi != [], s++, s = 0));
   write("/dev/stderr");
   [B[1], [Mat([Colrev(f, #b) | f <- b])^-1 | b <- B[2]], Phi];
}

\\ Self-dual algebra defining the torsion subscheme (equivalently:
\\ Galois representation) E[m] for an elliptic curve  E  over  Q.
ellgalrep(E, m) =
{
   my(function, k, err);
   if(m == 2,
      for(k = -1, 2,
         if(!ellisoncurve(E, [k, 0]),
            return(ellgalrep_function(E, m, 'x - k)))));
   for(k = -1, 1,
      if(!ellisoncurve(E, [0, k]),
         function = 2*('y - k) + E.a1*'x;
         if(m % 2 == 0,
            function -= 'x);
         break));
   while(1, iferr(return(ellgalrep_function(E, m, function)),
                  err, write("/dev/stderr", err); function -= 'x));
}
