ellgalrep
=========

A program to compute Galois representations attached to elliptic
curves over the rational field.


Author
------

Peter Bruin, <P.J.Bruin@math.leidenuniv.nl>


Prerequisites
-------------

A recent version of PARI/GP, <http://pari.math.u-bordeaux.fr/>.


Installation
------------

The program can be run from the source directory; no compilation or
installation is needed.  Edit the Makefile if necessary to adjust the
variable `$(GP)` and to select which representations are computed by
default.


Usage
-----

The function `ellgalrep` can be called from PARI/GP as in the
following example:

    gp> ellgalrep(ellinit("11a1"), 2)
    %1 = [[t, t^3 - t^2 - t - 1], [Mat(1), [1, 0, 1; 0, 0, 1; 0, 1, 1]], [1/4, 3/4, -1/4, 1/4; 3/4, -3/4, 1/4, -1/4; -1/4, 1/4, -3/4, 7/4; 1/4, -1/4, 7/4, 5/4]]

Simply typing `make` in the source directory will compute all Galois
representations `E[m]` for one elliptic curve in each isogeny class of
conductor up to 128 and `m` a prime power less than 10.
