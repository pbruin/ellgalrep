# Makefile for Galois representations attached to elliptic curves

GP ?= gp -q

# small prime powers
M := "[2, 3, 4, 5, 7, 8, 9]"

# conductor bound
N := 128

all: curves

.PHONY: curves

curves: curves.txt
	$(MAKE) `cat $<`

curves.txt: Makefile
	echo "M = " $M "; forell(ell, 1, $N, E = ellinit(ell[2]); for(i = 1, #M, m = M[i]; print(ell[1], \"_\", m, \"_red.gp\")), 1)" | $(GP) > $@

.PRECIOUS: %_red.gp

%_red.gp:
	echo "apply(x -> print(x), ellgalrep(`echo $* | sed -e 's/\(.*\)_\(.*\)/ellinit("\1"), \2/'`));" | $(GP) ellgalrep.gp > $@
